import logging
from aiohttp import web
from init import init_app
from request_handler import RequestHandler
from async_db_connector import AsyncConnector
from middlewares import error_middleware


async def on_start_tasks(app: web.Application) -> None:
    # Create client session and pg connection for server.
    pg_connector = AsyncConnector(app['database_settings']['address'],
                                  app['database_settings']['port'],
                                  app['database_settings']['database_name'],
                                  app['database_settings']['user'],
                                  app['database_settings']['password'])
    await pg_connector.create_pool()
    app['request_handler'] = RequestHandler(app, pg_connector)


async def on_shutdown_tasks(app: web.Application) -> None:
    await app['request_handler'].pg_connector.close_pool()


app = web.Application(middlewares=[error_middleware])
init_app(app)
logger = logging.getLogger(__file__)
logger.info("APP STARTED")
app.on_startup.append(on_start_tasks)
app.on_shutdown.append(on_shutdown_tasks)
logger.debug("background tasks added")

web.run_app(app, host=app['host'], port=app['port'])
