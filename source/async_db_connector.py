import aiopg
from aiohttp import web
import logging

logger = logging.getLogger(__file__)


class AsyncConnector(object):
    def __init__(self,
                 host: str,
                 port: str,
                 database: str,
                 user: str,
                 password: str):
        self.dsn = 'dbname=%s user=%s password=%s host=%s port=%s' % (database, user, password, host, port)
        self.pool = None

    async def create_pool(self) -> None:
        self.pool = await aiopg.create_pool(self.dsn)

    async def close_pool(self) -> None:
        self.pool.close()

    async def insert_user(self,
                          name: str,
                          coord_x: float,
                          coord_y: float) -> int:
        async with self.pool.acquire() as conn:
            async with conn.cursor() as cur:
                sql = "INSERT INTO public.users (name, coords) VALUES (%s, 'POINT(%s %s)'::geography) RETURNING id;"
                await cur.execute(sql, (name, coord_x, coord_y))
                row = await cur.fetchone()
                if row:
                    return row[0]
        logger.debug("cant get id from db")
        raise web.HTTPServiceUnavailable()

    async def get_users(self,
                        coord_x: float,
                        coord_y: float,
                        limit: int):
        answer = []
        async with self.pool.acquire() as conn:
            async with conn.cursor() as cur:
                sql = "SELECT name FROM public.users ORDER BY coords <-> st_point(%s, %s) limit %s"
                await cur.execute(sql, (coord_x, coord_y, limit))
                rows = await cur.fetchall()
                for row in rows:
                    answer.append({"name": row[0]})
        return answer
