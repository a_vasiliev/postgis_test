import logging
import utils
from aiohttp import web

from async_db_connector import AsyncConnector

logger = logging.getLogger(__file__)


class RequestHandler(object):
    def __init__(self,
                 app: web.Application,
                 pg_connector: AsyncConnector):
        self.pg_connector = pg_connector
        self.__add_routes(app)

    def __add_routes(self, app: web.Application):
        # GET
        app.router.add_get('/users/get', self.__get_users)

        # POST
        app.router.add_post('/users/add', self.__add_user)

    async def __add_user(self, request: web.Request) -> web.Response:
        body = await request.json()
        try:
            name = str(body["name"])
            coord_x = float(body["x"])
            coord_y = float(body["y"])
        except (KeyError, ValueError, TypeError) as e:
            logger.debug(e)
            raise web.HTTPUnprocessableEntity()
        user_id = await self.pg_connector.insert_user(name, coord_x, coord_y)
        return web.json_response({"done": True, "id": user_id})

    async def __get_users(self, request: web.Request) -> web.Response:
        try:
            coord_x = float(request.query.get("x"))
            coord_y = float(request.query.get("y"))
            number_of_neighbours = int(request.query.get("N", utils.MAX_NUMBER_OF_NEIGHBOURS))
        except (KeyError, ValueError, TypeError) as e:
            logger.debug(e)
            raise web.HTTPUnprocessableEntity()
        if number_of_neighbours > utils.MAX_NUMBER_OF_NEIGHBOURS or number_of_neighbours < 0:
            number_of_neighbours = utils.MAX_NUMBER_OF_NEIGHBOURS
        answer = await self.pg_connector.get_users(coord_x, coord_y, number_of_neighbours)
        return web.json_response(answer)

