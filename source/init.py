import logging
import json
from configparser import ConfigParser
from argparse import ArgumentParser, RawTextHelpFormatter
import random
import time
import sys
import os
from aiohttp import web


def __parse_args():
    parser = ArgumentParser(add_help=True,
                            formatter_class=RawTextHelpFormatter)
    parser.add_argument('-c', '--config', '--ini',
                        type=__get_config,
                        dest='config',
                        help='path to config file with ini format\n'
                             'Schema:\n'
                             '[server_settings]\n'
                             'host - mandatory\n'
                             'port - mandatory\n'
                             'database_settings - mandatory\n'
                             'log_level - optional\n')
    return parser.parse_args()


def __get_config(path: str) -> dict:
    config = ConfigParser()
    config.read(path)
    return config.__dict__['_sections']


def __init_logs(log_level: str) -> None:
    root = logging.getLogger()
    root.setLevel(logging.getLevelName(log_level))
    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.getLevelName(log_level))
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    root.addHandler(ch)


def init_app(app: web.Application) -> None:
    args = __parse_args()
    __init_logs(args.config['server_settings'].get('log_level', 'DEBUG'))
    app['database_settings'] = json.loads(args.config['server_settings'].get('database_settings'))
    app['host'] = args.config['server_settings'].get('host')
    app['port'] = int(args.config['server_settings'].get('port'))

    random.seed(os.getpid() + time.time())
