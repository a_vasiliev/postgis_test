#!/bin/bash
docker stop test-app
docker stop test-db
docker rm test-app
docker rm test-db
docker run --name test-db -d -p 7891:5432 -e POSTGRES_PASSWORD=12345 test-postgis-db
sleep 30
docker run --name test-app -d --net=host test-postgis-app
sleep 5
