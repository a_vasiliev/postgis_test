SET search_path TO public;
create table users (
    id serial primary key,
    coords geography, -- longitude and latitude
    name varchar(100)
);
create index users_coords_index on users using gist (coords);
